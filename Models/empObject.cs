﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace old.Models
{
    public class empObject
    {
        
        public bool success_login { get; set; }
        public int id { get; set; }
        public string nname { get; set; }
        public string username { get; set; }
        public int pharmaCode { get; set; }
        public string emaill { get; set; }
        public string manager { get; set; }
        public string department { get; set; }

        public List<accessGranted_Object> accessGranted { get; set; }
    }
}