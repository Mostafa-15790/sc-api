﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace old.Models
{
    public class memberObject
    {
        public string mem_id { get; set; }
        public string mem_name { get; set; }

        public string mem_mobile_bupa { get; set; }
        public string mem_mobile { get; set; }
        public string mem_YearOfBirth { get; set; }
        public string mem_policyID { get; set; }

        public string mem_policyName { get; set; }


        public string mem_gender { get; set; }
        public string insCompany { get; set; }

        public string mem_EffectiveFrom { get; set; }
        public string mem_EffectiveTo { get; set; }
        public string mem_Deductible { get; set; }
        public string mem_ClassName { get; set; }

        public string insID { get; set; }       

        public double limit { get; set; }
        public string lastTrans { get; set; }
        public double amount { get; set; }
        public string store { get; set; }
        public double available_limit { get; set; }
    }
}