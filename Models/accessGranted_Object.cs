﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace old.Models
{
    public class accessGranted_Object
    {
        public int function_id { get; set; }
        public string role_name { get; set; }
        public string function_name { get; set; }
        public string Afunction_name { get; set; }
        public string function_type { get; set; }
        public string node_order { get; set; }
        public string function_path { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }

        public string controller { get; set; }
        public string targett { get; set; }
        public string icons { get; set; }
        public string projects { get; set; }
    }
}