﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Security;
using System.Web.Http;
using old.bupaService;
using old.Models;
using System.Web.Mvc;
using System.DirectoryServices;
using Oracle.ManagedDataAccess.Client;
using System.Data;

namespace old.Controllers
{
    public class ValuesController : ApiController
    {
        string SERVER = "user id=15790;password=Mor12345;Data Source=(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = Whjed-Rtl-scan.nmc.com)(PORT = 1521))(CONNECT_DATA = (SERVICE_NAME = RMSP)))";

        // GET api/values
        public IEnumerable<string> Get()
        {
           
            return new string[] { "value1", "value2" };
        }
        [System.Web.Http.Route("api/Values/getBupaMember")]
        [System.Web.Http.HttpPost]
        public dynamic getBupaMember(dynamic dynamicRequest)
        {
            String errorr = "";
            bool success = false;
            var memberObj = new memberObject();
            string Requested_member_id = dynamicRequest.Requested_member_id;
            string Requested_insurance_code = dynamicRequest.Requested_insurance_code;
            string insurance_code = "";
            //string bob = nameof(enum_Insurance_Provider_Code.insurance_IDs.buba);
            var val = (int)Enum.Parse(typeof(enum_Insurance_Provider_Code.insurance_IDs), enum_Insurance_Provider_Code.insurance_IDs.buba.ToString());
            if (Requested_insurance_code == val.ToString())
            {
                insurance_code = ((int)Enum.Parse(typeof(enum_Insurance_Provider_Code.insurance_IDs_ProviderCode), enum_Insurance_Provider_Code.insurance_IDs_ProviderCode.buba.ToString())).ToString();
            }
            try
            {
                var client = new POINTServiceClient();
                client.ClientCredentials.UserName.UserName = "NMCLive";
                client.ClientCredentials.UserName.Password = "nmcLIVEp@ssw0rD";
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.PeerOrChainTrust;
                ServicePointManager.ServerCertificateValidationCallback += ((sender, certificate, chain, sslPolicyErrors) => true);

                //Task<TableOfBenefits> isEligible = client.VerifyMemberAsync("18166287", "");17251386
                //TableOfBenefits mem_data1 = client.VerifyMember("18166287", "22671");

                TOB_OS mem_data = client.VerifyMemberAsOS(Requested_member_id, insurance_code, DepartmentCode.PHA, true);
                if (mem_data.IsMemberEligible == false)
                {
                    errorr = "User not Eligable";
                }
                else
                {                    
                    memberObj.mem_id = mem_data.MembershipNo.Trim();
                    memberObj.mem_name = mem_data.MemberName.Trim();
                    memberObj.mem_mobile_bupa = mem_data.MobileNumber.Trim().Substring(1);
                    memberObj.mem_mobile = "+966" + mem_data.MobileNumber.Trim().Substring(1);
                    memberObj.mem_gender = mem_data.Gender.Trim();
                    memberObj.mem_policyID = mem_data.ContratNO.Trim();
                    memberObj.mem_policyName = mem_data.CustomerName.Trim();
                    memberObj.insCompany = "Bupa";
                    memberObj.mem_YearOfBirth = mem_data.YearOfBirth.Trim();
                    memberObj.mem_EffectiveFrom = mem_data.EffectiveFrom.ToString().Trim();
                    memberObj.mem_EffectiveTo = mem_data.EffectiveTo.ToString().Trim();
                    memberObj.mem_Deductible = mem_data.Deductible.ToString().Trim();
                    memberObj.mem_ClassName = mem_data.ClassName.Trim();
                    success = true;
                    errorr = "";
                }
            }
            catch (Exception e)
            {
                errorr = e.Message;
            }
            //return memberObj;
            return Json(new { success = success, errorr = errorr, memberObj = memberObj});

        }
        // GET api/values/5
        [System.Web.Http.Route("api/Values/LdapAccess")]
        [System.Web.Http.HttpPost]
        public dynamic LdapAccess(dynamic dynamicRequest)
        {
            //DataSet1.S_LOGINS.Fill,GetData 
            //var countt = test1.Employees.Count;
            old.Models.DataSet1TableAdapters.S_LOGINSTableAdapter x = new Models.DataSet1TableAdapters.S_LOGINSTableAdapter();
            old.Models.DataSet1TableAdapters.S_ROLESTableAdapter Roles = new Models.DataSet1TableAdapters.S_ROLESTableAdapter();
            DataTable tt = x.GetData();
            test1.Employees = tt.Rows;
            test1.Employee = tt.Rows.Find("alsaggaf.ah");

            String errorr = "";
            bool success = false;
            empObject empobject=new empObject();
            var accessGranted_List = new List<accessGranted_Object>();
            string Requested_emp_id = dynamicRequest.Requested_emp_id;
            string Requested_password = dynamicRequest.Requested_password;
           
            try
            {
                empObject empobject1 = IsAuthenticated("LDAP://prjed-dc03.nmc.com:389", Requested_emp_id, Requested_password);
                empobject = empobject1;
                if (empobject.success_login == true)
                {
                    // return View();
                    errorr = "";
                    success = true;
                    string queryString = "select pos.s_logins.user_name,pos.s_roles.role_name,pos.s_functions.* from pos.s_logins inner join pos.s_roles on pos.s_roles.role_code=pos.s_logins.roles_code"
+" inner join pos.s_roles_d on pos.s_roles_d.roles_code = pos.s_logins.roles_code"
+ " inner join pos.s_functions on  pos.s_functions.function_id = pos.s_roles_d.function_id  where user_name = :user_name";
                    using (OracleConnection connection = new OracleConnection(SERVER))
                    {
                        OracleCommand command = new OracleCommand(queryString)
                        {
                            Connection = connection
                        };
                        try
                        {
                            connection.Open();
                            command.Parameters.Add(":user_name", Requested_emp_id);
                            OracleDataReader rdr = command.ExecuteReader();
                            if (rdr.HasRows == true)
                            {
                                success = true;
                                while (rdr.Read())
                                {
                                    accessGranted_Object accessGranted_Object = new accessGranted_Object();
                                    accessGranted_Object.function_id = int.Parse(rdr["function_id"].ToString());
                                    accessGranted_Object.function_name = rdr["function_name"].ToString();
                                    accessGranted_Object.Afunction_name = rdr["Afunction_name"].ToString();
                                    accessGranted_Object.function_type = rdr["function_type"].ToString();
                                    accessGranted_Object.function_path = rdr["function_path"].ToString();
                                    accessGranted_Object.node_order = rdr["node_order"].ToString();                                  
                                    accessGranted_Object.role_name = rdr["role_name"].ToString();
                                    accessGranted_Object.created_date = rdr["created_date"].ToString();
                                    accessGranted_Object.created_by = rdr["created_by"].ToString();
                                    accessGranted_List.Add(accessGranted_Object);
                                }
                                empobject.accessGranted = accessGranted_List;
                            }
                            else
                            {
                                success = true;
                                errorr = "";
                            }
                            rdr.Close();
                        }
                        catch (Exception ex)
                        {
                            success = false;
                            errorr = ex.Message;
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
                else
                {
                    // return View();
                    // return RedirectToAction("SignIn", "Pages");CN
                    errorr = "User Login Details Failed !";
                    success = false;
                }
            }
            catch (Exception e)
            {
                errorr = e.Message;
            }
            //return memberObj;
            return Json(new { success = success, errorr = errorr, empObject = empobject });

        }
        public empObject IsAuthenticated(string ldap, string usr, string pwd)
        {
            empObject emp = new empObject();
            bool authenticated = false;
            emp.success_login = false;
            try
            {
                DirectoryEntry entry = new DirectoryEntry(ldap, usr, pwd);
                object nativeObject = entry.NativeObject;

                DirectorySearcher dssearch = new DirectorySearcher(ldap);
                dssearch.Filter = "(sAMAccountName=" + usr + ")";
                SearchResult sresult = dssearch.FindOne();
                DirectoryEntry dsresult = sresult.GetDirectoryEntry();
                var FirstName = dsresult.Properties["givenName"][0].ToString();
                var LastName = dsresult.Properties["sn"][0].ToString();
                var Email = dsresult.Properties["mail"][0].ToString();
                var Manager = dsresult.Properties["manager"][0].ToString();
                var Department = dsresult.Properties["department"][0].ToString();

                emp.username = usr;
                emp.nname = FirstName + " " + LastName;
                emp.emaill = Email;
                emp.manager = Manager;
                emp.department = Department;
                emp.success_login = true;
                System.Web.HttpContext.Current.Session["empObject"] = emp;

                authenticated = true;
            }
            catch (DirectoryServicesCOMException cex)
            {
                Console.WriteLine(cex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return emp;
        }

        [System.Web.Http.Route("api/Values/Insurance_GetAll")]
        [System.Web.Http.HttpPost]
        public dynamic Insurance_GetAll(dynamic dynamicRequest)
        {
            String errorr = "";
            bool success = false;
            var insurance_co_list = new List<insurance_co>();
            try
            {
               
                string queryString = "select * from corp_owner.nmc_ins_m_com";
                using (OracleConnection connection = new OracleConnection(SERVER))
                {
                    OracleCommand command = new OracleCommand(queryString)
                    {
                        Connection = connection
                    };
                    try
                    {
                        connection.Open();
                        OracleDataReader rdr = command.ExecuteReader();
                        if (rdr.HasRows == true)
                        {
                            success = true;
                            while (rdr.Read())
                            {
                                insurance_co insurance_co = new insurance_co();
                                insurance_co.ins_id = int.Parse(rdr["INS_CO_ID"].ToString());
                                insurance_co.ins_name = rdr["INS_CO_NAME"].ToString();
                                insurance_co.co_is_INTG = int.Parse(rdr["INS_CO_IS_INTG"].ToString());
                                insurance_co_list.Add(insurance_co);
                            }
                        }
                        else
                        {
                            success = true;
                            errorr = "";
                        }
                        rdr.Close();
                    }
                    catch (Exception ex)
                    {
                        success = false;
                        errorr = ex.Message;
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception e)
            {
                errorr = e.Message;
            }
            //return memberObj;
            return Json(new { success = success, errorr = errorr, insurance_co = insurance_co_list });

        }

        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
